Project Management

    :sparkles: Updated for 2021 evaluation standards!

    Build a personal site describing you and your final project.
    Upload it to the class archive.
    Work through a git tutorial.

Learning outcomes

    Explore and use website development tools
    Identify and utilise version control protocols

Have you answered these questions?

    Made a website and described how you did it
    Introduced yourself
    Documented steps for uploading files to archive
    Pushed to the class archive
    Signed and uploaded Student Agreement


[![Project Management Video](https://img.youtube.com/vi/HpWc3LyJIMc/0.jpg)](https://www.youtube.com/watch?v=HpWc3LyJIMc)
