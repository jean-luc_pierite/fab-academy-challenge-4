Computer-Aided Design

    :sparkles: Updated for 2021 evaluation standards!

    Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress your images and videos, and post it on your class page

Learning outcomes

    Evaluate and select 2D and 3D software
    Demonstrate and describe processes used in modelling with 2D and 3D software

Have you answered these questions?

    Modelled experimental objects/part of a possible project in 2D and 3D software
    Shown how you did it with words/images/screenshots
    Included your original design files

[![CAD pt 1](https://img.youtube.com/vi/3CWGulq1Maw/0.jpg)](https://www.youtube.com/watch?v=3CWGulq1Maw)

[![CAD pt 2](https://img.youtube.com/vi/TNFrpymmoo4/0.jpg)](https://www.youtube.com/watch?v=TNFrpymmoo4)
