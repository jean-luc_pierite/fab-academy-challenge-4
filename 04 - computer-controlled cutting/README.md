Computer-Controlled Cutting

    :sparkles: Updated for 2021 evaluation standards!

    Group assignment:
        characterize your lasercutter's focus, power, speed, rate, kerf, and joint clearance
        document your work (individually or in group)

    Individual assignments
        Design, lasercut, and document a parametric press-fit construction kit, which can be assembled in multiple ways. Account for the lasercutter kerf.
        cut something on the vinylcutter

Learning outcomes

    Demonstrate and describe parametric 2D modelling processes
    Identify and explain processes involved in using the laser cutter.
    Develop, evaluate and construct the parametric construction kit
    Identify and explain processes involved in using the vinyl cutter.

Have you answered these questions?

    linked to the group assignment page
    Explained how you parametrically designed your files
    Documented how you made your press-fit kit
    Documented how you made your vinyl cutting
    Included your original design files
    Included your hero shots


## LCInterlocking - FreeCAD Laser Cut Interlocking Module

![Link to Github](https://github.com/execuc/LCInterlocking)
