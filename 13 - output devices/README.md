![LCD1602A](https://datasheetspdf.com/pdf-file/519149/CA/LCD1602A/1)

I had several rounds of debugging the LCD display.

I originally started with this tutorial which adds a button to control the backlight:
https://www.fischco.org/technica/2018/status-screen/

I was able to get the backlight and contrast working with a button and potentiometer, but could not get text to show. I ultimately followed the wiring instructions at Adafruit:
https://learn.adafruit.com/character-lcds/wiring-a-character-lcd

With this tutorial I got garbled text each time, which led to to learn about nibble loss causes synch issues between the lcd and the microcontroller.

I noticed that Neil had pull up resistors going to the pins in the example circuit. So, I rewired and added in the resistors:
http://academy.cba.mit.edu/classes/output_devices/LCD/hello.LCD.44.png

Two suggestions from this post finally gave me the results that I was looking for. On the hardware side, I added a capacitor to Pins 1 & 2 of the LCD. On the software side, I reinitiate the LCD screen every ten seconds. I’ll have to further tweak the code to provide live results of the final project.
