# Micro Challenge IV MDEF - Abundance

## Restoration Ecology and Technology

![](images/restoration-ecology-slide.png)

## Sketch of Project

![](images/delta-planter.jpg)

## Restoration Ecology, Vertical Garden Questionnaire

- Q: What are your initial impressions of the delta robot vertical farm?
- A: I don't understand really well how the extrusion process works, without a second explanation. The Open Agriculture initiative from MIT has been deactivated since last year. The food computer that they wanted to build was very intersting.
- Q: Can you explain to me the food computer?
- A: The food computer is like a box. It is a computer for growing plants. On the bottom you have an acrylic with  a few holes to place the substrate. On the top there are LEDs. There is also a humidifier. The point is for people to grow their own food in their own homes.
- Q: If you could grow your own food in your home, why would you do that?
- A: I don't see people spending hundreds of euros on a device to grow food. My uncle has a "nano garden".
- Q: Explain a nano garden to me?
- A: He has an indoor garden that grows plants with an LED, an irrigation system, and the herbs grow for you to put in dishes. The price is 80 euros. Only high-class people are willing to pay for a device like that. If you did a food computer, it would make something very special. Solar-power would be a good feature.
- Q: Do you think that this causes a disparity in health?
- A: It could, but it's all about designing. If someone makes 80 euros, it doesn't make sense to spend it all on a device to grow herbs. A food computer would need to grow something much more important. Bioreactors that grow spirulina are very interesting. It's a niche market, but people could add at it to their coffee. There are many different uses for this type of devices. Feed for livestock for example. Don't make people grow something that is a garnish, but something that is good for their health and for the sustainability of the planet.
- Q: It sounds like there are herbs and plants that humans could benefit from greater awareness. What species should be promoted more?
- A: Algae, duckweed, (also an aquatic plant), mushrooms, microgreens, microorganisms, vegetable proteins. It doesn't make sense to grow soy.
- Q: In your own opinion why does it not make sense to grow soy?
- A: Soy is a big plant, and it is not that convenient to grow. It's possible but what do you do with soy? With algae or duckweed, you don't need to process them into food products. They could be added directly to coffee or smoothies. They can be grown on water as well. Soy also doesn't open up a conversation. Soy is a commodity. Let them deforest more patches of land in the Amazon. Is soy the best thing that we should eat? If I would be able to grow anything, it would be duckweed. It's very small and grows well in the right conditions. If you put it on the rooftop, you could add a solar panel and then it could be a closed system with lights and humidity control to grow duckweed like crazy. People would have to harvest it everyday, and they could even further process it into a powder or flour.

## Sensors List 

**TEMPERATURE & HUMIDITY:** DHT11

Measurement: Temperature & Humidity 

Unit: ºC ; % 
              
**LIGHT:** uFire PAR Sensor
 
Measurement: Photosynthetically Active Radiation (PAR)

Unit: μmol/m²/s

**ELECTRIC CONDUCTIVITY:** Grove EC Sensor Kit

Measurement: Electrical Conductivity 

Unit: ms/cm (millisiemens per centimeter)

**PH:** Grove PH Sensor

Measurement: Potential of Hydrogen (PH)

Unit: pH

## Duckweed 

### Cultivation

**Water Temperature:** 
Range: 6C - 33C
Ideal: 20C - 30C

**Light Intensity:** 200 µMol - 250 µMol (Low to Moderate Lighting Requirement)

**pH:** 
Range: 5 - 9
Ideal: 6.5 - 7

**Nutrients:** 
Compost Tea 
Crude Sea Salt: 1 gram

**Perfect Habitat:** Wetland

**Water Aeration/Oxygenation:** 
_"Don’t forget to add oxygen. If you place duckweed in a pool without aeration the water will eventually go anaerobic. Without oxygen, nothing will grow."_

**Water Movement:**
_"Duckweed prefers slow moving water however, you will notice fastest growth with areas of agitation. Slight agitation, such as where an outlet flows, seems to increase the asexual reproductive process. The duckweed divide quicker."_


**Water Depth:**
Optimal: 10 - 15 cm

Sources:
https://gardenpool.org/online-classes/growing-duckweed



### Harvesting

Daily Harvesting: 100 g/m^2/day

### Nutrition

NUTRITIONAL CONTENT:

* Dry Weight: 4% - 8%
* Protein: 35% - 45%
* Fat: 4% - 7%
* Starch: 4% - 10%
* Lutein: 70 MG/100 G
* Violaxanthin: 46 MG/100 G
* Beta-Carotene: 28 MG/100 G
* Zeaxanthin: 4.3 MG/100 G
* Alpha-Tocopherol: 9 MG/100 G
* Gamma-Tocopherol: 13 MG/100 G

Source: https://www.researchgate.net/publication/307630118_Nutritional_value_of_duckweeds_Lemnaceae_as_human_food


INEXPENSIVE PROTEIN SOURCE

VERY HIGH GROWTH RATE 

HIGHLY TASTY

RISING POPULARITY OF VEGAN/VEGETARIAN DIET & LIFESTYLE IN DEVELOPED COUNTRIES 

PERFECT FOR INDOOR FARMING (CONTROLLED ENVIRONMENT, CLEAN & SAFE ENVIRONMENTAL CONDITIONS,...)

### Why Indoor Farming?

"_Duckweed fronds can double their mass in two days under ideal conditions of nutrient availability, sunlight, and temperature. This is faster than almost any other higher plant. Under experimental conditions their production rate can approach an extrapolated yield of four metric tons/ha/day of fresh plant biomass, or about 80 metric tons/ha/year of solid material. This pattern more closely resembles the exponential growth of unicellular algae than that of higher plants and denotes an unusually high biological potential._"

"_Average growth rates of unmanaged colonies of duckweed will be reduced by a variety of stresses: nutrient scarcity or imbalance: toxins, extremes of pH and temperature: crowding by overgrowth of the colony: and competition from other plants for light and nutrients._"

"_The solid fraction of a wild colony of duckweed growing on nutrient-poor water typically ranges from 15 to 25 percent protein and from 15 to 30 percent fiber. Duckweed grown under ideal conditions and harvested regularly will have a fiber content of 5 to 15 percent and a protein content of 35 to 45 percent, depending on the species involved..._"

"_Duckweed farming is a continuous process requiring intensive management for optimum production. Daily attention and frequent harvesting are needed throughout the year to ensure the productivity and health of the duckweed colonies._"

Source: https://www.researchgate.net/publication/242631709_Duckweed_Aquaculture_A_New_Aquatic_Farming_System_for_Developing_Countries

## Light Nutrition & Recipes

![Light Spectrum 1](images/Light_Spectrum_2.PNG)

![Light Spectrum 2](images/Light_Spectrum_3.PNG)

![Light Spectrum 3](images/The_Science_of_Indoor_Farming.PNG)

## Photographs of End Artifact

![Abundance 1](images/ABUNDANCE_1.1.jpg)

![Abundance 2](images/ABUNDANCE_1.2.jpg)

![Abundance 3](images/ABUNDANCE_1.3.jpg)

![Abundance 4](images/ABUNDANCE_1.4.jpg)

![Abundance 5](images/ABUNDANCE_1.5.jpg)

## Link to Website:
https://wordpress.com/page/sassyduckweedfarm.wordpress.com/5

  
